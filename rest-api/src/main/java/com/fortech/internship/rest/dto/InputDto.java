package com.fortech.internship.rest.dto;


/**
 * Created by internship on 18.07.2016.
 */
public class InputDto extends BaseDto {

    private long id;
    private ProductDto product;
    private ShopDto shop;
    private long quantity;

    public InputDto() {

    }

    public InputDto(ProductDto product, ShopDto shop, long quantity) {

        this.product = product;
        this.shop = shop;
        this.quantity = quantity;

    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public ProductDto getProduct() {
        return product;
    }

    public void setProduct(ProductDto product) {
        this.product = product;
    }

    public ShopDto getShop() {
        return shop;
    }

    public void setShop(ShopDto shop) {
        this.shop = shop;
    }

    public long getQuantity() {
        return quantity;
    }

    public void setQuantity(long quantity) {
        this.quantity = quantity;
    }
}
