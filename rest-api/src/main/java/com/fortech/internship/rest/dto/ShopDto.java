package com.fortech.internship.rest.dto;

/**
 * Created by internship on 18.07.2016.
 */
public class ShopDto extends BaseDto {
    private long id;
    private String name;

    public ShopDto(String name) {
        this.name = name;


    }

    public ShopDto() {

    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}

