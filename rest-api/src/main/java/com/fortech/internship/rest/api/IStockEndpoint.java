package com.fortech.internship.rest.api;

import com.fortech.internship.rest.dto.StockProduct;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * Created by internship on 20.07.2016.
 */
@RequestMapping(value = "/stock")
public interface IStockEndpoint {
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    List<StockProduct> getStock();
}
