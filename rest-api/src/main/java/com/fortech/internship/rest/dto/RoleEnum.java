package com.fortech.internship.rest.dto;

/**
 * Created by internship on 20.07.2016.
 */
public enum RoleEnum {
    ADMIN("ADMIN"), USER("USER");

    private String roleName;

    RoleEnum(String roleName) {
        this.roleName = roleName;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }
}

