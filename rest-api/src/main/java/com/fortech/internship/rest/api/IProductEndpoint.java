package com.fortech.internship.rest.api;

import com.fortech.internship.rest.dto.ProductDto;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * Created by internship on 18.07.2016.
 */
@RequestMapping(value = "/product")
public interface IProductEndpoint {
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    List<ProductDto> getProducts();

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    ResponseEntity<ProductDto> getProduct(@PathVariable long id);

    @RequestMapping(method = RequestMethod.POST)
    ResponseEntity<ProductDto> addProduct(@RequestBody ProductDto product);

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    ResponseEntity<ProductDto> deleteProduct(@PathVariable long id);

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    ResponseEntity<ProductDto> updateProduct(@PathVariable long id, @RequestBody ProductDto product);
}
