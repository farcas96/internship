package com.fortech.internship.rest.api;


import com.fortech.internship.rest.dto.InputDto;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * Created by internship on 18.07.2016.
 */
@RequestMapping(value = "/input")
public interface IInputEndpoint {
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    List<InputDto> getInputs();

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    ResponseEntity<InputDto> getInput(@PathVariable long id);

    @RequestMapping(value = "", method = RequestMethod.POST)
    ResponseEntity<InputDto> addInput(@RequestBody InputDto input);
}
