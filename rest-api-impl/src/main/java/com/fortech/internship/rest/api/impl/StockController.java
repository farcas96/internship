package com.fortech.internship.rest.api.impl;


import com.fortech.internship.rest.api.IStockEndpoint;
import com.fortech.internship.rest.dto.StockProduct;
import com.fortech.internship.service.api.impl.IStockService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by internship on 18.07.2016.
 */
@RestController
public class StockController implements IStockEndpoint {


    @Autowired
    IStockService stockService;

    @Override
    public List<StockProduct> getStock() {

        return stockService.getStock();
    }
}
