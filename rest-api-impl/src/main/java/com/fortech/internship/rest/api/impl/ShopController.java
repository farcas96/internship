package com.fortech.internship.rest.api.impl;


import com.fortech.internship.rest.api.IShopEndpoint;
import com.fortech.internship.rest.dto.ShopDto;
import com.fortech.internship.service.api.impl.IShopService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;


import java.util.List;

/**
 * Created by internship on 15.07.2016.
 */
@RestController
public class ShopController implements IShopEndpoint {

    @Autowired
    private IShopService shopService;

    @Override
    public List<ShopDto> getShops() {
        return shopService.getShops();
    }

    @Override
    public ResponseEntity<ShopDto> getShop(@PathVariable long id) {
        if (shopService.getShop(id)==null) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }

        return new ResponseEntity<>(shopService.getShop(id) , HttpStatus.OK);
    }

    @Override
    public ResponseEntity<ShopDto> addShop(@RequestBody ShopDto shop) {
        shopService.addShop(new ShopDto());

        return ResponseEntity.status(HttpStatus.CREATED).body(null);
    }

    @Override
    public ResponseEntity<ShopDto> deleteShop(@PathVariable long id) {
        if (!shopService.deleteShop(id)) {

            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
        shopService.deleteShop(id);

        return ResponseEntity.status(HttpStatus.NO_CONTENT).body(null);
    }

    @Override
    public ResponseEntity<ShopDto> updateShop(@PathVariable long id, @RequestBody ShopDto shop) {
        if (!shopService.updateShop(id)) {

            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
        ShopDto shopToUpdate = shopService.getShop(id);
        shopToUpdate.setName(shop.getName());
        shopService.getShop(id);

        return ResponseEntity.status(HttpStatus.NO_CONTENT).body(null);
    }




}
