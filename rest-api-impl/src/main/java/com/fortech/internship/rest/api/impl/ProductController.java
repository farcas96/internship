package com.fortech.internship.rest.api.impl;


import com.fortech.internship.rest.api.IProductEndpoint;
import com.fortech.internship.rest.dto.ProductDto;
import com.fortech.internship.service.api.impl.IProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;


import java.util.List;

/**
 * Created by internship on 15.07.2016.
 */
@RestController
public class ProductController implements IProductEndpoint {

    @Autowired
    private IProductService productService;


    @Override
    public List<ProductDto> getProducts() {
        return productService.getProducts();
    }

    @Override
    public ResponseEntity<ProductDto> getProduct(@PathVariable long id) {
        if (productService.getProduct(id) == null) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }

        return new ResponseEntity<>(productService.getProduct(id), HttpStatus.OK);
    }


    @Override
    public ResponseEntity<ProductDto> addProduct(@RequestBody ProductDto product) {
        productService.addProduct(new ProductDto());

        return ResponseEntity.status(HttpStatus.CREATED).body(null);
    }

    @Override
    public ResponseEntity<ProductDto> deleteProduct(@PathVariable long id) {
        if (!productService.deleteProduct(id)) {

            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
        productService.deleteProduct(id);

        return ResponseEntity.status(HttpStatus.NO_CONTENT).body(null);
    }

    @Override
    public ResponseEntity<ProductDto> updateProduct(@PathVariable long id, @RequestBody ProductDto product) {
        if (!productService.updateProduct(id)) {

            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }

        ProductDto productToUpdate = productService.getProduct(id);
        productToUpdate.setName(product.getName());
        productService.getProduct(id);

        return ResponseEntity.status(HttpStatus.NO_CONTENT).body(null);
    }
}
