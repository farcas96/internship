package com.fortech.internship.service.api.impl;

import com.fortech.internship.core.model.UserEntity;
import com.fortech.internship.rest.dto.UserDto;

import java.util.List;

/**
 * Created by internship on 20.07.2016.
 */

public interface IUserService {
    List<UserDto> getUsers();

    UserDto getUser(long id);

    boolean addUser(UserDto userDto);

    boolean updateUser(long id, UserDto user);

    boolean deleteUser(long id);
}

