package com.fortech.internship.service.api.impl;

import com.fortech.internship.rest.dto.OutputDto;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

/**
 * Created by internship on 18.07.2016.
 */
public interface IOutputService {
    List<OutputDto> getOutputs();

    OutputDto getOutput(long id);

    boolean addOutput(OutputDto output);
}
