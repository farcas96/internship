package com.fortech.internship.service.api.impl;

import com.fortech.internship.rest.dto.InputDto;

import java.util.List;

/**
 * Created by internship on 18.07.2016.
 */

public interface IInputService {
    List<InputDto> getInputs();

    InputDto getInput(long id);

    boolean addInput(InputDto input );

}
