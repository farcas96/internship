package com.fortech.internship.service.api.impl;

import com.fortech.internship.rest.dto.ManagerDto;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

/**
 * Created by internship on 18.07.2016.
 */
public interface IManagerService {
    List<ManagerDto> getManagers();

    ManagerDto getManager(long id);

    boolean addManager(ManagerDto manager);

    boolean updateManager(long id);

    boolean deleteManager(long id);
}
