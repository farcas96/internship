package com.fortech.internship.service.api.impl;

import com.fortech.internship.rest.dto.ProductDto;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

/**
 * Created by internship on 18.07.2016.
 */
public interface IProductService {
    List<ProductDto> getProducts();

    ProductDto getProduct(long id);

    boolean addProduct(ProductDto product);

    boolean updateProduct(long id) ;

    boolean deleteProduct(long id);

}
