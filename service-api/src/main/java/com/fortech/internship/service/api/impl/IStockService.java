package com.fortech.internship.service.api.impl;

import com.fortech.internship.rest.dto.StockProduct;

import java.util.List;

/**
 * Created by internship on 20.07.2016.
 */
public interface IStockService {
    void calculateStock();
    List<StockProduct> getStock();


}
