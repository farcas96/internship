package com.fortech.internship.service.api.impl;

import com.fortech.internship.rest.dto.ShopDto;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

/**
 * Created by internship on 18.07.2016.
 */
public interface IShopService {
    List<ShopDto> getShops();

    ShopDto getShop(long id);

    boolean addShop(ShopDto shop);

    boolean updateShop(long id);

    boolean deleteShop(long id);
}
