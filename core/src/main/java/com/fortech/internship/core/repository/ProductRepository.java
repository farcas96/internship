package com.fortech.internship.core.repository;

import com.fortech.internship.core.model.ProductEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by internship on 18.07.2016.
 */
public interface ProductRepository extends CrudRepository<ProductEntity,Long>{
    List<ProductEntity> findByName(String name);
}
