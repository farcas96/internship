package com.fortech.internship.core.repository;

import com.fortech.internship.core.model.InputEntity;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by internship on 18.07.2016.
 */
public interface InputRepository extends CrudRepository<InputEntity, Long> {

}
