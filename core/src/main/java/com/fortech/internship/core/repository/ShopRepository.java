package com.fortech.internship.core.repository;

import com.fortech.internship.core.model.ShopEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by internship on 18.07.2016.
 */
public interface ShopRepository extends CrudRepository<ShopEntity, Long> {
    List<ShopEntity> findByName(String name);
}