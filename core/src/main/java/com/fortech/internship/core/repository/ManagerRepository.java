package com.fortech.internship.core.repository;

import com.fortech.internship.core.model.ManagerEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by internship on 18.07.2016.
 */
public interface ManagerRepository extends CrudRepository<ManagerEntity, Long> {
    List<ManagerEntity> findByLastName(String lastName);
}
