package com.fortech.internship.core.repository;

import com.fortech.internship.core.model.InputEntity;
import com.fortech.internship.core.model.OutputEntity;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by internship on 18.07.2016.
 */
public interface OutputRepository extends CrudRepository<OutputEntity, Long> {

}
