package com.fortech.internship.core.model;

import com.fortech.internship.rest.dto.ProductDto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created by internship on 18.07.2016.
 */
@Entity
@Table(name = "products")
public class ProductEntity extends BaseEntity {

    @Column(name = "name")
    private String name;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ProductEntity product = (ProductEntity) o;

        return name.equals(product.name);

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + name.hashCode();
        return result;
    }

    public ProductEntity() {

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ProductEntity(String name) {
        this.name = name;


    }

    public String toString() {

        return String.format("Product[name='%s' ]", name);

    }

    public ProductDto toDto() {

        return new ProductDto(this.getId(), this.getName());


    }

    public ProductEntity updateFromDto(ProductDto dto) {
        this.name = dto.getName();

        return this;
    }

    public ProductEntity createFromDto(ProductDto dto) {

        this.setName(dto.getName());

        return this;
    }
}
