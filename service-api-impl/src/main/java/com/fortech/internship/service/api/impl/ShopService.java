package com.fortech.internship.service.api.impl;

import com.fortech.internship.core.model.ShopEntity;
import com.fortech.internship.core.repository.ShopRepository;
import com.fortech.internship.rest.dto.ShopDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by internship on 18.07.2016.
 */
@Service
public class ShopService implements IShopService {

    @Autowired
    ShopRepository shopRepository;

    @Override
    public List<ShopDto> getShops() {
        List<ShopDto> shops = new ArrayList<>();
        for (ShopEntity shopEntity : shopRepository.findAll())
            shops.add(shopEntity.toDto());

        return shops;
    }

    @Override
    public ShopDto getShop(long id) {
        if (!shopRepository.exists(id)) {

            return null;
        }

        return (shopRepository.findOne(id).toDto());
    }

    @Override
    public boolean addShop(ShopDto shop) {
        shopRepository.save(new ShopEntity().updateFromDto(shop));

        return true;
    }

    @Override
    public boolean updateShop(long id) {
        if (!shopRepository.exists(id)) {

            return false;
        }
        ShopEntity shopToUpdate = shopRepository.findOne(id);
        shopRepository.save(shopToUpdate);

        return true;
    }

    @Override
    public boolean deleteShop(long id) {
        if (!shopRepository.exists(id)) {

            return false;
        }
        shopRepository.delete(id);

        return true;
    }
}