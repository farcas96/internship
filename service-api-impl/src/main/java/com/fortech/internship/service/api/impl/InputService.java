package com.fortech.internship.service.api.impl;

import com.fortech.internship.core.model.InputEntity;
import com.fortech.internship.core.model.ProductEntity;
import com.fortech.internship.core.model.ShopEntity;
import com.fortech.internship.core.repository.InputRepository;
import com.fortech.internship.core.repository.ProductRepository;
import com.fortech.internship.core.repository.ShopRepository;
import com.fortech.internship.rest.dto.InputDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;


import java.util.ArrayList;
import java.util.List;

/**
 * Created by internship on 18.07.2016.
 */
@Service
public class InputService implements IInputService {

    @Autowired
    InputRepository inputRepository;
    @Autowired
    ShopRepository shopRepository;
    @Autowired
    ProductRepository productRepository;

    @Override
    public List<InputDto> getInputs() {
        List<InputDto> inputs = new ArrayList<>();
        for (InputEntity input  : inputRepository.findAll())
            inputs.add(input.toDto());

        return inputs;
    }

    @Override
    public InputDto getInput(long id) {
        if (!inputRepository.exists(id)) {

            return null;
        }

        return (inputRepository.findOne(id).toDto());
    }

    @Override
    public boolean addInput(InputDto input) {
        InputEntity inputEntity = new InputEntity();
        ShopEntity shop = shopRepository.findByName(input.getShop().getName()).get(0);
        ProductEntity productEntity = productRepository.findByName(input.getProduct().getName()).get(0);

        inputEntity.setShop(shop);
        inputEntity.setProduct(productEntity);
        inputEntity.setLong(input.getQuantity());

        inputRepository.save(inputEntity);

        return true;
    }


}

