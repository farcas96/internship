package com.fortech.internship.service.api.impl;

import com.fortech.internship.core.model.InputEntity;
import com.fortech.internship.core.model.OutputEntity;
import com.fortech.internship.core.model.ProductEntity;
import com.fortech.internship.core.model.ShopEntity;
import com.fortech.internship.core.repository.OutputRepository;
import com.fortech.internship.core.repository.ProductRepository;
import com.fortech.internship.core.repository.ShopRepository;
import com.fortech.internship.rest.dto.OutputDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.ArrayList;
import java.util.List;



/**
 * Created by internship on 18.07.2016.
 */
@Service
public class OutputService implements IOutputService {

   @Autowired
    OutputRepository outputRepository;
    @Autowired
    ShopRepository shopRepository;
    @Autowired
    ProductRepository productRepository;

    @Override
    public List<OutputDto> getOutputs() {
        List<OutputDto> outputs = new ArrayList<>();
        for (OutputEntity output  : outputRepository.findAll())
            outputs.add(output.toDto());

        return outputs;
    }

    @Override
    public OutputDto getOutput(long id) {
        if (!outputRepository.exists(id)) {
            return null;
        }

        return (outputRepository.findOne(id).toDto());
    }

    @Override
    public boolean addOutput(OutputDto output) {
        OutputEntity outputEntity= new OutputEntity();
        ShopEntity shop = shopRepository.findByName(output.getShop().getName()).get(0);
        ProductEntity productEntity = productRepository.findByName(output.getProduct().getName()).get(0);
        outputEntity.setShop(shop);
        outputEntity.setProduct(productEntity);
        outputEntity.setLong(output.getQuantity());
        outputRepository.save(outputEntity);

        return true;
    }
}

