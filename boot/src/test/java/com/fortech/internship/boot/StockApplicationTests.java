
package com.fortech.internship.boot;

import com.fortech.internship.core.model.InputEntity;
import com.fortech.internship.core.model.OutputEntity;
import com.fortech.internship.core.model.ProductEntity;
import com.fortech.internship.core.model.ShopEntity;
import com.fortech.internship.core.repository.InputRepository;
import com.fortech.internship.core.repository.OutputRepository;
import com.fortech.internship.core.repository.ProductRepository;
import com.fortech.internship.core.repository.ShopRepository;
import com.fortech.internship.rest.api.impl.InputController;
import com.fortech.internship.rest.api.impl.OutputController;
import com.fortech.internship.rest.api.impl.StockController;
import com.google.gson.Gson;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.mock.http.MockHttpOutputMessage;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.FilterChainProxy;
import org.springframework.security.web.context.HttpSessionSecurityContextRepository;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Arrays;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;


public class StockApplicationTests extends AbstractTestBase {

    @Autowired
    InputRepository inputRepository;
    @Autowired
    InputController inputController;
    @Autowired
    OutputRepository outputRepository;
    @Autowired
    OutputController outputController;
    @Autowired
    ProductRepository productRepository;
    @Autowired
    ShopRepository shopRepository;
    @Autowired
    StockController stockController;


    @After
    public void afterMethod() {
        inputRepository.deleteAll();
        outputRepository.deleteAll();
    }

    @Before
    public void createTest() {
        ProductEntity productOne = productRepository.save(new ProductEntity("Pen"));
        ShopEntity shopOne = shopRepository.save(new ShopEntity("Lidl"));


        inputRepository.save(new InputEntity(productOne, shopOne, 20));
        outputRepository.save(new OutputEntity(productOne, shopOne, 15));
    }

    @Test
    public void getAllInputs() throws Exception {


        mockMvc.perform(get("/stock/list").session(admin())
                .accept(MediaType.APPLICATION_JSON))
//                .andDo(print())
                .andExpect(status().isOk());

    }
}
