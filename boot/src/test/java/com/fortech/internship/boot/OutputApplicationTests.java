package com.fortech.internship.boot;

import com.fortech.internship.core.model.InputEntity;
import com.fortech.internship.core.model.OutputEntity;
import com.fortech.internship.core.model.ProductEntity;
import com.fortech.internship.core.model.ShopEntity;
import com.fortech.internship.core.repository.OutputRepository;
import com.fortech.internship.core.repository.ProductRepository;
import com.fortech.internship.core.repository.ShopRepository;
import com.fortech.internship.rest.api.impl.OutputController;
import com.fortech.internship.rest.dto.InputDto;
import com.fortech.internship.rest.dto.OutputDto;
import com.google.gson.Gson;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.context.annotation.Bean;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.mock.http.MockHttpOutputMessage;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.FilterChainProxy;
import org.springframework.security.web.context.HttpSessionSecurityContextRepository;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Arrays;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

/**
 * Created by internship on 15.07.2016.
 */

public class OutputApplicationTests extends AbstractTestBase {


    private MediaType contentType = new MediaType(MediaType.APPLICATION_JSON.getType(),
            MediaType.APPLICATION_JSON.getSubtype(),
            Charset.forName("utf8"));


    @Autowired
    OutputRepository outputRepository;
    @Autowired
    OutputController outputController;
    @Autowired
    ProductRepository productRepository;
    @Autowired
    ShopRepository shopRepository;
    OutputEntity initialOutput;
    OutputEntity newOutput;
    Long randomId = 1L;

    @After
    public void afterMethod() {
        outputRepository.deleteAll();
    }
    @After
    public void tearDown() {
        outputRepository.deleteAll();
    }

    @Before
    public void createTest() {
        ShopEntity shop = new ShopEntity("shop");
        shopRepository.save(shop);
        ProductEntity productEntity = new ProductEntity("product");

        productRepository.save(productEntity);
        outputRepository.save(new OutputEntity(productRepository.findOne(productEntity.getId()), shop, 15L));

        ProductEntity productOne = productRepository.save(new ProductEntity("Chair"));
        ShopEntity shopOne = shopRepository.save(new ShopEntity("Lidl"));


        initialOutput = outputRepository.save(new OutputEntity(productOne, shopOne, 3));

        newOutput = new OutputEntity(productOne, shopOne, 7L);


    }

    @Test
    public void getAllOutputs() throws Exception {


        mockMvc.perform(get("/output/list").session(admin())
                .accept(MediaType.APPLICATION_JSON))
                //.andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void getOneOutput() throws Exception {


        this.mockMvc.perform(get("/output/" + initialOutput.getId()).session(admin()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.product.name", is(initialOutput.getProduct().getName())))
                .andExpect(jsonPath("$.shop.name", is(initialOutput.getShop().getName())));


    }
    @Test
    public void getOneOutputNotFound() throws Exception {


        this.mockMvc.perform(get("/output/" + 5L).session(admin()))
                .andExpect(status().isNotFound());
    }

    @Test
    public void createOneOutput() throws Exception {


        String json = json(newOutput.toDto());


        mockMvc.perform(post("/output").session(admin())
                .contentType(MediaType.APPLICATION_JSON)
                .content((json)))
               // .andDo(print())
                .andExpect(status().isCreated());

    }

    @Test
    public void updateOneOutput() throws Exception {

        ProductEntity productOne = productRepository.save(new ProductEntity("Pen"));
        ShopEntity shopOne = shopRepository.save(new ShopEntity("Lidl"));
        OutputEntity initialOutput = outputRepository.save(new OutputEntity(productOne, shopOne, 15));


        this.mockMvc.perform(get("/output" + "/" + initialOutput.getId()).session(admin()))
                .andExpect(jsonPath("$.product.name", is(initialOutput.getProduct().getName())))
                .andExpect(jsonPath("$.shop.name", is(initialOutput.getShop().getName())));


    }

    @Test
    public void deleteOneOutput() throws Exception {

        this.mockMvc.perform(delete("/output" + "/" + randomId).session(admin()))
                .andExpect(status().isMethodNotAllowed());
    }

    @Test
    public void deleteOneOutputNotFound() throws Exception {


        this.mockMvc.perform(get("/output/" + 5L).session(admin()))
                .andExpect(status().isNotFound());
    }

    private String json(Object o) throws IOException {

        return new Gson().toJson(o);
    }


}
