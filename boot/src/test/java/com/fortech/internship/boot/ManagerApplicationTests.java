package com.fortech.internship.boot;

import com.fortech.internship.core.model.InputEntity;
import com.fortech.internship.core.model.ManagerEntity;
import com.fortech.internship.core.model.ProductEntity;
import com.fortech.internship.core.model.ShopEntity;
import com.fortech.internship.core.repository.ManagerRepository;
import com.fortech.internship.rest.api.impl.ManagerController;
import com.fortech.internship.rest.dto.ManagerDto;
import com.fortech.internship.rest.dto.ProductDto;
import com.google.gson.Gson;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.mock.http.MockHttpOutputMessage;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.FilterChainProxy;
import org.springframework.security.web.context.HttpSessionSecurityContextRepository;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Arrays;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

/**
 * Created by internship on 15.07.2016.
 */

public class ManagerApplicationTests extends AbstractTestBase {

    private MediaType contentType = new MediaType(MediaType.APPLICATION_JSON.getType(),
            MediaType.APPLICATION_JSON.getSubtype(),
            Charset.forName("utf8"));

    @Autowired
    ManagerRepository managerRepository;
    @Autowired
    ManagerController managerController;
    ManagerEntity initialManager;
    ManagerDto managerDto;

    @After
    public void tearDown() {
        managerRepository.deleteAll();
    }

    @After
    public void afterMethod() {
        managerRepository.deleteAll();
    }

    @Before
    public void createTest() {
        managerRepository.save(new ManagerEntity("Jack", "Bauer"));
        managerRepository.save(new ManagerEntity("Chloe", "O'Brian"));

        initialManager = managerRepository.save(new ManagerEntity("Jack", "Bauer"));

        managerDto = new ManagerDto("JSDF", "sadas");

    }

    @Test
    public void getAllManagers() throws Exception {


        mockMvc.perform(get("/manager/list").session(admin())
                .accept(MediaType.APPLICATION_JSON))
                //.andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void getOneManager() throws Exception {


        this.mockMvc.perform(get("/manager/" + initialManager.getId()).session(admin()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.firstName", is(initialManager.getFirstName())))
                .andExpect(jsonPath("$.lastName", is(initialManager.getLastName())));
    }

    @Test
    public void getOneManagerNotFound() throws Exception {


        this.mockMvc.perform(get("/manager/" + 5L).session(admin()))
                .andExpect(status().isNotFound());
    }

    @Test
    public void createOneManager() throws Exception {


        mockMvc.perform(post("/manager/").session(admin())
                .contentType(MediaType.APPLICATION_JSON)
                .content(json(managerDto)))
               // .andDo(print())
                .andExpect(status().isCreated());
    }

    @Test
    public void updateOneManager() throws Exception {


        this.mockMvc.perform(get("/manager" + "/" + initialManager.getId()).session(admin()))
                .andExpect(jsonPath("$.firstName", is(initialManager.getFirstName())));

    }

    @Test
    public void deleteOneManagerNotFound() throws Exception {

        this.mockMvc.perform(delete("/manager/" + 7L).session(admin()))
                .andExpect(status().isNotFound());
    }


    @Test
    public void deleteOneManager() throws Exception {


        this.mockMvc.perform(delete("/manager/" + initialManager.getId()).session(admin()))
                .andExpect(status().isNoContent());
    }


    private String json(Object o) throws IOException {

        return new Gson().toJson(o);
    }


}
