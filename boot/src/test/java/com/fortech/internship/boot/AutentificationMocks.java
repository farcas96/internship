package com.fortech.internship.boot;

import com.fortech.internship.core.model.UserEntity;
import com.fortech.internship.rest.dto.RoleEnum;
import org.springframework.security.authentication.TestingAuthenticationToken;
import org.springframework.security.core.Authentication;

/**
 * Created by internship on 22.07.2016.
 */
public class AutentificationMocks {

    static Authentication userAuthentication() {
        UserEntity user = new UserEntity("Lucian","password", RoleEnum.ADMIN);
        return new TestingAuthenticationToken(user, null, "ROLE_USER");
    }

    static Authentication adminAuthentication() {
        UserEntity user  = new UserEntity("Farcas","password" ,RoleEnum.USER);
        return new TestingAuthenticationToken(user, null, "ROLE_ADMIN");
    }
}
