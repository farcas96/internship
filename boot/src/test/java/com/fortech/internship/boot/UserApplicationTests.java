package com.fortech.internship.boot;

import com.fortech.internship.core.model.UserEntity;
import com.fortech.internship.core.repository.UserRepository;
import com.fortech.internship.rest.api.impl.UserController;
import com.fortech.internship.rest.dto.RoleEnum;
import com.fortech.internship.rest.dto.UserDto;
import com.google.gson.Gson;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.FilterChainProxy;
import org.springframework.security.web.context.HttpSessionSecurityContextRepository;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.io.IOException;
import java.nio.charset.Charset;


import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


/**
 * Created by internship on 22.07.2016.
 */

public class UserApplicationTests extends AbstractTestBase {


    private MediaType contentType = new MediaType(MediaType.APPLICATION_JSON.getType(),
            MediaType.APPLICATION_JSON.getSubtype(),
            Charset.forName("utf8"));

    @Autowired
    UserRepository userRepository;
    @Autowired
    UserController userController;
    UserEntity initialUser;
    UserDto userDto;


    @After
    public void afterMethod() {
        userRepository.deleteAll();
    }

    @After
    public void tearDown() {
        userRepository.deleteAll();
    }

    @Before
    public void createTest() {
        userRepository.save(new UserEntity("Jack", "Bauer", RoleEnum.ADMIN));
        userRepository.save(new UserEntity("Chloe", "O'Brian", RoleEnum.USER));

        initialUser = userRepository.save(new UserEntity("Jack", "Bauer", RoleEnum.ADMIN));

        userDto = new UserDto("asdsa", "JSDF", RoleEnum.ADMIN);
    }

    @Test
    public void getAllUsers() throws Exception {


        mockMvc.perform(get("/user/list").session(admin())
                .accept(MediaType.APPLICATION_JSON))
                //.andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void getOneUser() throws Exception {


        this.mockMvc.perform(get("/user/" + initialUser.getId()).session(admin()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.username", is(initialUser.getUsername())));
    }
    @Test
    public void getOneUsertNotFound() throws Exception {


        this.mockMvc.perform(get("/user/" + 5L).session(admin()))
                .andExpect(status().isNotFound());
    }
    @Test
    public void createOneUser() throws Exception {


        mockMvc.perform(post("/shop/").session(admin())
                .contentType(MediaType.APPLICATION_JSON)
                .content(json(userDto)))
                //.andDo(print())
                .andExpect(status().isCreated());
    }


    @Test
    public void updateOneUser() throws Exception {


        this.mockMvc.perform(get("/user" + "/" + initialUser.getId()).session(admin()))
                .andExpect(jsonPath("$.username", is(initialUser.getUsername())));

    }

    @Test
    public void deleteOneUser() throws Exception {


        this.mockMvc.perform(delete("/user" + "/" + initialUser.getId()).session(admin()))
                .andExpect(status().isNoContent());


    }

    @Test
    public void deleteOneUserNotFound() throws Exception {


        this.mockMvc.perform(get("/user/" + 5L).session(admin()))
                .andExpect(status().isNotFound());

    }

    private String json(Object o) throws IOException {

        return new Gson().toJson(o);
    }

}

